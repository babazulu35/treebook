# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Treebook::Application.config.secret_key_base = '1730e02544d7f3522c2a16564a339f33663774a84b75dc730f02cfc69888bd1795f1cb6ce3e6fbf443adcdafbeb21bfae638fc22a7733d9dd6cc2b5a2da525c3'
